// SubdomainPart.cpp SubdomainPart validation class implementation
// Written by Varun Patel

#include "SubdomainPart.h"

// Default Constructor
SubdomainPart::SubdomainPart()
{}

// Init constructor
SubdomainPart:: SubdomainPart(const String& address)
{
	Address = address;
}


// Validates the Address and returns true when the Address is valid
bool SubdomainPart::IsValid()
{
	
	//Check for valid characters
	if (Address.FindFirstNotOf(VALID_SUBDOMAIN_CHARS) != NOT_FOUND)
		return false;

	int lengthOfDomain = Address.GetLength();
	if( (lengthOfDomain < MIN_DOMAIN_SIZE) || (lengthOfDomain > MAX_DOMAIN_SIZE) )
		return false;

	// use the CheckSpecialCharacterRule Function to test whether the 
	// first character & last character in the SubdomainPart is a special character
	// also checks for two special symbols in a row
	return
		CheckSpecialCharacterRule('.', Address) &&
		CheckSpecialCharacterRule('-', Address);
	
	return true;
}

