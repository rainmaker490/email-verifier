// LocalPart.h LocalPart validation class declaration
// Written by Varun Patel

#pragma once
#include "String.h"
#include "Vector.h"
#define VALID_LOCAL_PART_CHARS "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890.-'+_"
#define MIN_LOCAL_PART_SIZE 1
#define MAX_LOCAL_PART_SIZE 64

using namespace std;

// CheckSpecialCharacterRule Function - Tests for rules regarding special characters
bool CheckSpecialCharacterRule(char symbol, String parts);

class LocalPart
{
public:
	// Default constructor, REQUIRED
	LocalPart() {}

	// Plays a role of the Init-constructor
	void Set(const String& address);

	// Validates the Address and returns true when the Address is valid
	bool IsValid();

private:
	// LocalPart address
	String Address;
};
