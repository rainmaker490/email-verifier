// SubdomainPart.h SubdomainPart validation class declaration
// Written by Varun Patel

#pragma once
#include <iostream>
#include "Vector.h"
#include "String.h"
#define MIN_DOMAIN_SIZE 1
#define MAX_DOMAIN_SIZE 253
#define VALID_SUBDOMAIN_CHARS "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-"


using namespace std;

// CheckSpecialCharacterRule Function - Tests for rules regarding special characters
bool CheckSpecialCharacterRule(char symbol, String parts);

class SubdomainPart
{
public:
	// Default constructor
	SubdomainPart();

	// Init constructor
	SubdomainPart(const String& address);

	// Validates the Address and returns true when the Address is valid
	virtual bool IsValid();

protected:
	// SubdomainPart address
	String Address;
};