// TldPart.h TldPart validation class declaration
// Written by Varun Patel

#pragma once

#include <sstream>
#include <fstream>
#include <string>

#include "SubdomainPart.h"
#include "Vector.h"
#include "String.h"

using namespace std;

class TldPart : public SubdomainPart
{
public:
	// Default Constructor
	TldPart() {}

	// Init constructor
	void Set(const String& address);

	// Validates the Address and returns true when the Address is valid
	bool IsValid();

private:
	void LoadTlds();

	Vector<String> ValidTLDs;

};
