// TldPart.cpp TldPart validation class implementation
// Written by Varun Patel

#include "TldPart.h"

// Plays a role of the init constructor
void TldPart::Set(const String& address)
{
	Address = address;
}


// Validates the Address and returns true when the Address is valid
bool TldPart::IsValid()
{
	LoadTlds();
	bool _TLDIsValid = false;

	// Compare TldPart with vector of all ValidTLDs
	for (int i=0; i < ValidTLDs.GetSize() && !_TLDIsValid; i++)
	{
		if (ValidTLDs[i] == Address)
			_TLDIsValid = true;
	}

	return _TLDIsValid;
}

void TldPart::LoadTlds()
{
	String validTLDs;
	
	string myString;
	ifstream input("ValidTLD.txt");

	// Check to see if the ValidTLDs file exists. If the file doesnot exist, cout an error
	if (input.fail())
	{
		cout << "Opening ValidTLD file failed" << endl;
		exit (0);
	}

	// store ValidTLDs in a vector
	int j = 0;
	while (!input.eof())
	{
		getline(input, myString);
		validTLDs= myString.c_str();

		for (int i = 0; i < validTLDs.GetLength(); i++)
			validTLDs[i] = tolower(validTLDs[i]);
	
		ValidTLDs.Insert(validTLDs, j);
		j++;
	}
}