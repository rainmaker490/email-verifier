// String.h - String Class declaration
// Written by Varun Patel - Notes & Lab

#pragma once

#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

#define NOT_FOUND -1

// ASCII character string
// For every non tempate class that you create you typically have a header file and implement in a cpp file
// For all template class- it goes in the header- you cannot compile template code and store it into an object file
class String
{
public:
	// Default constructor
	String();

	// Whenever you have a class that has pointers as data members, the class must have a Copy Constructor
	// Must have a copy constructor to perform deep copy
	String(const String& source);

	// Init- constructor for initializing this string with a C-string
	String(const char* text);

	// Init- constructor for allocating the String of a Specified size
	String(int size);

	// Destructor- Whenever you have a class that has pointers as data members- it should have a Copy constructor AND a Destructor
	// Must have a destructor
	~String(); // Only one destructor even though you have more than one constructor

	// Must have assignment operator to perform deep-copy
	String& operator = (const String& source);
	// Assigns C-string to this String
	String& operator = (const char* text);

	// Index operator to get or set individual characters
	// Returns a reference to a single character from this string
	char& operator [] (int index) const;

	// Comparison operator - Implement & Test
	bool operator == (const String& compareTo) const;
	bool operator != (const String& compareTo) const;
	bool operator > (const String& compareTo) const;	
	bool operator < (const String& compareTo) const;

	// Concatenation operator
	// Returns a new String, which is this String merged with aString
	String operator + (const String& aString);

	// Returns the index of aChar starting from startPosition or
	// returns NOT_FOUND when aChar is not found
	// throws an exception when startPosition is outside the bounds
	int Find(char aChar, int startPosition = 0) const;

	int FindLastOf(char aChar) const;
	int FindFirstNotOf(const char* text);


	// Returns a substring of this String beginning at startPosition and length characters long
	// if length is not specified, then the substring is from startPosition until the end of this String
	// throws an exception when startPosition or length is outside the bounds
	String Substring (int startPosition, int length = 0) const;


	// Returns the count of characters in the String
	int GetLength() const;

	// Returns the count of characters in the argument
	// static means the function has nothing to do with the data members of the class
	// String::GetLength("skjdhfakjhfsa")
	static int GetLength(const char* text);

	// Gets the contents of the Text private data member- Read only get -accessor to return text
	char* GetText() const;

private:
	// Encapsulated C-String 
	char* Text; // Use Pointer to shrink size as needed. 

	// Error reporting function. returns void, but ocassionally throws an exception
	// Takes 2 arguments, index and c-String
	void CheckBounds(int index, const char* source) const;

};

// Stream output operator to write a string to output stream
ostream& operator << (ostream& out, const String& aString);