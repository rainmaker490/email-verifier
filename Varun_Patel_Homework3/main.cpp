// main.cpp - Email validation program main entry point
// Written by Varun Patel

#include <iostream>
#include <istream>
#include <fstream>
#include <string>

#include "Email.h"
#include "String.h"

using namespace std;


// Validates emailAddress and returns true if it is valid. Returns zero
// otherwise
bool IsValid(String email);


void main()
{
	// 1. Open input and output files
	ifstream input("Email.txt");
	
	
	// Check to see if the input file exists. If it doesn't cout error.
	if (input.fail())
	{
		cout << "Opening input file failed" << endl;
		system ("pause");
	}
	
	ofstream output("Result.txt");

	// 2. While not at the end of the input file
	while ( !input.eof() )
	{
		String emailAddress;
		string myString; 

		// 3. Read an email address from the input file
		// "cheat" , use getline to read emailAddress from file
		// convert to String

		getline(input, myString);
		 
		// Email is an email validation object
		emailAddress = myString.c_str();

		Email email(emailAddress);

		// 4. Validate the email address
		bool isValid = email.IsValid();

		// 5. If the email address is valid write 1 to the output file
		//    else write zero
		output << isValid << '\t' << email << endl;

		// 6. Repeat
	}

	system("pause");
}

