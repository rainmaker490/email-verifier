// Email.cpp Email validation class implementation
// Written by Varun Patel

#include "Email.h"

// Init constructor
Email::Email(const String& address)
{
	Address = address;
}

//Parses the Address string into Local and Domain
bool Email::Parse()
{
	// use find to find the '@'
	int atIndex = Address.Find('@');

	if( atIndex == 0 )
		return false;

	if ( atIndex == string::npos)
		return false;

	// populate the Local and Domain data variables with the parsed data
	Local.Set(Address.Substring(0, atIndex-1));
	Domain.Set(Address.Substring(atIndex +1));

	return true;
}

// Validates the Address and returns true when the Address is valid
bool Email::IsValid()
{
	return
		Parse() &&
		Local.IsValid() &&
		Domain.IsValid();
}

// Read only access to Address for ostream
String Email::GetAddress() const
{
	return Address;
}

// Stream output operator to write a string to output stream
ostream& operator << (ostream& out, const Email& aString)
{
	return out << aString.GetAddress();
}