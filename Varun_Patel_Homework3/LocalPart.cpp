// LocalPart.cpp LocalPart validation class implementation
// Written by Varun Patel

#include "LocalPart.h"

// Plays a role of the Init-constructor
void LocalPart::Set(const String& address)
{
	Address = address;
}

// Validates the Address and returns true when the Address is valid
bool LocalPart::IsValid()
{
	// Check the length of the LocalPart
	int localPartLength = Address.GetLength();
	if( (localPartLength < MIN_LOCAL_PART_SIZE) || (localPartLength > MAX_LOCAL_PART_SIZE) )
		return false;

	// Check for valid characters in the LocalPart
	if (Address.FindFirstNotOf(VALID_LOCAL_PART_CHARS) != NOT_FOUND)
		return false;

	// use the CheckSpecialCharacterRule Function to test whether the 
	// first character & last character in the SubdomainPart is a special character
	// also checks for two special symbols in a row
	return
		CheckSpecialCharacterRule('.', Address) &&
		CheckSpecialCharacterRule('-', Address);

	return true;
}
