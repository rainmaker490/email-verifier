// String.cpp - String Class declaration
// Written by Varun Patel - Notes & Lab

#include "String.h"


// Default constructor
String::String()
{
	Text = NULL;
}

// Copy Constructor
String::String(const String& source)
{
	Text = NULL;
	// Calls the assignment operator to perform deep copy
	*this = source;
}



// Init- constructor 
String::String(const char* text)
{
	Text = new char;
	*this = text;
}


// Init- constructor for allocating the String of a Specified size
String::String(int size)
{
	// Allocate memory on heap using the new operator
	Text = new char[size];
	// heap - way of allocating memory for program ; memory persists until program teminates
	// use new to allocate memory with heap

}

// Destructor
String::~String()
{
	delete[] Text; // Deletes Heap allocated memory to prevent memory leak and program from crashing
}

// Check Bounds- error checking tool that internally checks for errors regarding bounds
void String::CheckBounds(int index, const char* source) const
{
	if ( index < 0 || (index > GetLength(source)))
	{
		String error(" [] Index: out of Bounds ");
		throw error;
	}
}

// Assignment operator to perform deep copy
String& String::operator = (const String& source)
{
	*this = source.GetText();
	return *this;
}


// Assignment operator to assign a C-string to this String
String& String::operator = (const char* text)
{
	// delete the existing string first
	delete[] Text;

	// +1 accounts for the NULL-terminator
	int trueLength = GetLength(text) + 1;

	// Allocate new memory, trueLength = Length + 1 (NULL-terminator)
	Text = new char[trueLength];

	// Copy all characters from source into Text (including the NULL-terminator)
	for (int i=0; i< trueLength; i++)
		Text[i] = text[i];

	return *this;
	// Deep Copy- allocate memory as well as copy the contents of that memory
}


// Index operator to get or set individual characters
// myString[i] = 's'
char& String::operator [] (int index) const
{	
	int length = GetLength();
	
	if( index < 0 || index >= length)
	{
		stringstream error;
		error << "String::operator [] - index " << index << 
			" is out of bounds (0.." << (length -1) << ")";
		throw String(error.str().c_str());
	}

	return Text[index];
}
 

// Returns the count of characters in the String
int String::GetLength() const
{
	return GetLength(Text);
}


// Returns the count of characters in the argument
int String::GetLength(const char* text)
{
	// For Loop scanning for null terminator
	int i;
	for(i = 0; text[i] != '\0'; i++);
	return i;
}



// Gets the contents of the Text private data member.
char* String::GetText() const
{
	return Text;
}


// Looks for a character in a String an returns its index if found
int String::Find(char aChar, int startPosition) const
{
	// Use the CheckBounds function to check the bounds of startPosition
	this->CheckBounds(startPosition, Text);

	// for loop to search through Text; breaks out of loop at NULL-terminator
	for(int i=startPosition; Text[i] != '\0'; i++)
	{
		// If statement  returns the index in Text where aChar is found
		if(Text[i] == aChar)
			return i;
	}

	// NOT_FOUND is defined as -1
	return NOT_FOUND;
}


int String::FindLastOf(char aChar) const
{
	int index = GetLength(Text);
	CheckBounds(index, Text);

	while (Text[index] != aChar && index >= 0)
		index--;
	if (index == NOT_FOUND)
		return NOT_FOUND;
	else 
		return index;
}

int String::FindFirstNotOf(const char* text)
{
	for (int i = 0; i < GetLength(); i++)
	{
		bool symbolFound = false;
		for (int j = 0; text[j] != '\0' && !symbolFound; j++)
			if (Text[i] == text[j])
				symbolFound = true;

		if (!symbolFound)
			return i;
	}
	return NOT_FOUND;
}



String String::Substring (int startPosition, int length) const
{

	this->CheckBounds(startPosition + length, Text);
	this->CheckBounds(startPosition, Text);	
	int lengthOfSubString;
	if(length == 0)
	{
		lengthOfSubString = GetLength() - startPosition + 1;
	}
	else
	{
		lengthOfSubString = length + 2;
	}
	char* substring = new char [lengthOfSubString];

	for(int i = startPosition; i < startPosition + lengthOfSubString - 1; i++)
		substring[i - startPosition] = Text[i];
	substring[lengthOfSubString - 1] = '\0';

	return String(substring);
}

// Adds two or more Strings together
String String::operator + (const String& aString)
{
	// define length and trueLength - These will hold the value of the length of strings
	// Makes program more efficient [Instead of calling GetLength() everywhere]
	int length = (*this).GetLength();
	int trueLength = aString.GetLength() + length + 1;

	// Allocate the space necessary of the new String that will be created
	char* addTwoStrings = new char [trueLength];
	
	// Use a for loop to copy the characters from Text into allocated memory of addTwoStrings
	for (int i=0; i < length; i++)
		addTwoStrings[i] = Text[i];

	// Use a second for loop to copy characters from 'aString' into allocated memory of addTwoStrings
	// the starting index of the aString in addTwoStrings is length.
	// aString[i-length] - need to subtract length since i = length in for loop.
	for (int i = length; i < trueLength - 1; i++)
		addTwoStrings[i] = aString[i-length];

	// Place Null Terminator at the end of addTwoStrings
	addTwoStrings[trueLength-1] = '\0';

	// Calls Init constructor- because of char*
	return String(addTwoStrings);
	
}

// Comparing if two Strings are equal. First check to see if the length of each are equal
// If lengths are equal, check to see if each characters in both strings are equal.
// First character that doesnt match indicates two non equal strings
 bool String::operator == (const String& compareTo) const
{
	// if the lengths of two strings are different, the strings are not equal
	// Case 1
	int length = GetLength();
	if (length != compareTo.GetLength())
		return false;

	// Compare each character. Break out of loop when the first
	// character does not equal
	for (int i = 0; i < length ;i++)
		if (Text[i] != compareTo[i])
			return false;

	return true;
}

 //  Compare if two Strings are not equal to each other (or equal)
bool String::operator != (const String& compareTo) const
{
	return 
		!(*this == compareTo);
}

// Compare the lengths of two Strings - Greater '<' than operator
bool String::operator > (const String& compareTo) const
{
	int length = GetLength();

	// Go through the each characters in both strings and compare ASCII val
	for (int i = 0; i < length; i++)
		if (Text[i] > compareTo[i])
			return true;

	return false;
}

// Compare the lengths of two Strings - Less '<' than operator
bool String::operator < (const String& compareTo) const
{
	return
		!(*this > compareTo);
}


// Stream output operator to write a string to output stream
ostream& operator << (ostream& out, const String& aString)
{
	return out << aString.GetText();
}




