// DomainPart.h DomainPart validation class declaration
// Written by Varun Patel

#pragma once

#include "String.h"
#include "Vector.h"
#include "SubdomainPart.h"
#include "TldPart.h"
#define MIN_DOMAIN_SIZE 1
#define MAX_DOMAIN_SIZE 253

#include "CheckSpecialCharacterRule.h"

using namespace std;

class DomainPart
{
public:
	// Default constructor, REQUIRED
	DomainPart() {}

	// Plays a role of the Init-constructor
	void Set(const String& address);

	// Validates the Address and returns true when the Address is valid
	bool IsValid();

private:
	// Parses the domain into Subdomains and TLD
	bool Parse();

	// Validates each Subdomains
	bool SubdomainsAreValid();

	// DomainPart address
	String Address;

	Vector<SubdomainPart> Subdomains;
	TldPart Tld;
};
