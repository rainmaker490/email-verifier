// CheckSpecialCharacterRule.h - Special Character Rule Checking Function declaration
// Written by Varun Patel

#pragma once
#include "Vector.h"
#include "String.h"

using namespace std;

bool CheckSpecialCharacterRule(char symbol, String parts);

