// Email.h Email validation class declaration
// Written by Varun Patel

#pragma once

#include "Vector.h"
#include "String.h"
#include "LocalPart.h"
#include "DomainPart.h"

using namespace std;

class Email
{
public:
	// Init constructor
	Email(const String& address);

	// Validates the Address and returns true when the Address is valid
	bool IsValid();

	// Read only access to Address for ostream
	String Email::GetAddress() const;

private:
	
	//Parses the Address string into Local and Domain
	bool Parse();

	// Email address
	String Address;

	// Parts of email address
	LocalPart Local;
	DomainPart Domain;

};
// Stream output operator to write a string to output stream
ostream& operator << (ostream& out, const Email& aString);