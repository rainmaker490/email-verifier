// CheckSpecialCharacterRule.cpp - Special Character Rule Checking Function
// Written by Varun Patel

#include "CheckSpecialCharacterRule.h"

// CheckSpecialCharacterRule checks to see if the first & last character in
// each 'parts' being tested [Subdomain & LocalPart] is a symbol
// also returns false if two symbols are consecutive in each tested 'parts'
bool CheckSpecialCharacterRule(char symbol, String parts)
{
	// Get the Length of the 'parts' - store this value in partsLength 
	int partsLength = parts.GetLength();

	// Check to see if at the first index of 'parts' being tested, the character is a symbol
	// return false if the first character is a symbol
	if (parts[0] == symbol)
		return false;

	// Check to see if at the last index of the 'parts' being tested, the character is a symbol
	// return false if the last character is a symbol
	if (parts[partsLength - 1] == symbol)
		return false;
	
	// Check to see if there are two special characters in a row in the string 'parts'
	// return false if there are two special characters in a row
	int symbolIndex = parts.Find(symbol);

	// partsLength minus one indicates the last index of the 'parts' string
	// If the Index of the symbol found is not equal to the last index of the 'parts' string,
	// check to see if the next character is also a symbol
	if (symbolIndex != string::npos)
	{
		if (parts[symbolIndex + 1] == symbol)
			return false;
	}

	return true;

}