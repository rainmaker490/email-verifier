// DomainPart.cpp DomainPart validation class implementation
// Written by Varun Patel

#include "DomainPart.h"

// Plays a role of the Init-constructor
void DomainPart::Set(const String& address)
{
	Address = address;
}

// Parses the domain into Subdomains and TLD
bool DomainPart::Parse()
{
	// look for '.', get a substring corresponding to the subdomain
	// use find_last_of to find the '.' ; the string after the last '.' (if found)
	// becomes your TLD
	int dotIndex = Address.FindLastOf('.');
	if ( dotIndex == string::npos)
		return false;

	// if dotIndex = 0, then return false
		if ( dotIndex == 0)
			return false;

	// and store it in a vector of Subdomains
	while (dotIndex != string::npos)
	{
		// use push_back function to get a subdomain from subdomain part into vector
		Subdomains.Add(SubdomainPart(Address.Substring(0, dotIndex - 1)));

		// Copy the remaining subdomain part into Address
		Address = Address.Substring(dotIndex + 1);

		// reset the dotIndex
		dotIndex = Address.Find('.');

		// Repeat
	}


	// the last subdomain is actually a Tld, so store it in a Tld object [Address]
	Tld.Set(Address);

	return true;
}


// Validate each Subdomains	
bool DomainPart::SubdomainsAreValid()
{
	// If a single subdomain is invalid than return false
	for (unsigned int i = 0; i < Subdomains.GetSize(); i++)
		if ( !Subdomains[i].IsValid() )
			return false;
	return true;
}


// Validates the Address and returns true when the Address is valid
bool DomainPart::IsValid()
{
	return
		// Parses the domain into Subdomains and TLD
		Parse() &&
		SubdomainsAreValid()&&
		Tld.IsValid();
}
